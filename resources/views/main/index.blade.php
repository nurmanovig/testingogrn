@extends('layout')

@section("css")
    <style>
        .form-section{
            border-radius: 10px;
            box-shadow: 0 0 0 10px #aaa;
            position: relative;
            text-align: center;
            margin: 100px auto;
            padding: 10px;
        }
    </style>
@endsection

@section('content')
    <div class="form-section col-6">
        <form method="post" action="/">
            {{csrf_field()}}
            <div class="form-group">
                <label for="number_input">ОГРН</label>

                <input type="text"
                       name="number"
                        value="{!! \App\Helpers\FormHelper::getValue($data, 'number')!!}"
                       class="form-control {!! \App\Helpers\FormHelper::validateClass($errors, 'number') !!}"
                       id="number_input"
                       placeholder="ВВедите ОГРН">
                <small class="text-danger">{!! \App\Helpers\FormHelper::getError($errors, 'number')!!}</small>
            </div>
            <div class="form-group">
                <?= captcha_img('mini')?>
                <input name="captcha"
                       id="number_input"
                       class="form-control {!! \App\Helpers\FormHelper::validateClass($errors, 'captcha') !!}"
                       placeholder="ВВедите каптча">
                    <small class="text-danger">{!! \App\Helpers\FormHelper::getError($errors, 'captcha')!!}</small>
            </div>
            <button type="submit" class="btn btn-primary">Найти</button>
        </form>
    </div>
@endsection
