@extends('layout')

@section("css")
    <link rel="stylesheet" href="/datepicker/css/bootstrap-datepicker.css">
    <style>
        .info-section{
            border-radius: 10px;
            box-shadow: 0 0 0 10px #aaa;
            position: relative;
            text-align: center;
            margin: 100px auto;
            padding: 10px;
        }

        form{
            text-align: center;
            padding: 20px;
            border:1px dotted;
        }
    </style>

@endsection

@section('content')
    <div class="info-section col-6">
        <h3>{{$number}}</h3>

        <form action="/info">
            <div class="form-group">
                <input type="text"
                       name="date_req"
                       placeholder="Дата"
                       value="{!! \App\Helpers\FormHelper::getValue($data, 'date_req') !!}"
                       class="form-control col-6 {!! \App\Helpers\FormHelper::validateClass($errors, 'date_req') !!}"
                       id="datepicker">

                <small class="text-danger">{{\App\Helpers\FormHelper::getError($errors, 'date_req')}}</small>
            </div>
            <div class="form-group">
                <select name="code"
                        class="form-control col-6 {!! \App\Helpers\FormHelper::validateClass($errors, 'code') !!}" id="">
                    <option value="" selected disabled>Коды</option>
                    <?php $value = \App\Helpers\FormHelper::getValue($data, 'code')?>
                    @foreach(\App\Helpers\ValuteCodes::$list as $code => $name)
                        <option value="{!! $code !!}" {!! $value==$code ? 'selected' : '' !!}>{{$name}}</option>
                    @endforeach
                </select>

                <small class="text-danger">{{\App\Helpers\FormHelper::getError($errors, 'code')}}</small>
            </div>
            <button type="submit" class="btn btn-success">Поиск</button>
        </form>

        @if($course != null )
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Название</th>
                        <th scope="col">Значение</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$course->name}}</td>
                        <td>{{$course->value}}</td>
                    </tr>
                </tbody>
            </table>
        @endif

        <a href="/" class="btn btn-info">Назад</a>

    </div>
@endsection
@section("js")
    <script src="/datepicker/js/bootstrap-datepicker.js"></script>

    <script>
        $('#datepicker').datepicker({
            format: "dd/mm/yyyy",
        });
    </script>
@endsection
