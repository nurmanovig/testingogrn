<?php


namespace App\Helpers;


use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class FormHelper
{
    public static function getValue(array $data, $key){
        return isset($data[$key]) ? $data[$key] : null;
    }

    public static function getError( $errors, $key){
        if($errors == null){
            return null;
        }

        return $errors->first($key);
    }

    public static function validateClass( $errors, $key){

        if($errors == null){
            return null;
        }
        return $errors->first($key) ? 'is-invalid' : 'is-valid';
    }
}