<?php


namespace App\Helpers;


class ValuteCodes
{
    public static $list = [
        'USD' => 'Доллар США',
        'BYR' =>  'Белорусский рубль',
        'EUR' => 'Евро'
    ];
}