<?php


namespace App\Rules;


use App\Helpers\ValuteCodes;
use Illuminate\Contracts\Validation\Rule;

class CourseValueRule implements Rule
{


    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // TODO: Implement passes() method.
        return array_key_exists($value, ValuteCodes::$list);
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return "Не валидный";
        // TODO: Implement message() method.
    }
}