<?php


namespace App\Rules;


use Illuminate\Contracts\Validation\Rule;

class CourseDateRule implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // TODO: Implement passes() method.
        return preg_match('/^\d{2}\/\d{2}\/\d{4}$/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        // TODO: Implement message() method.
        return "Дата не валидный (dd/mm/yyyy)";
    }
}