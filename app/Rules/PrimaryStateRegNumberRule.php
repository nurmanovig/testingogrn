<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PrimaryStateRegNumberRule implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // TODO: Implement passes() method.
        //В введенном коде, если это ОГРН, должны остаться только цифры
        $value = trim($value);

        //Производится проверка по длине введенного кода
        if(!preg_match("/\d{13}/", $value)){
            return false;
        }
        //Проверка год номера на валидность
        $year = substr($value, 1, 2) * 1;
        $max_year = date('y');
        echo $max_year;
        $min_year = '80';

        if(($year > $max_year && $year < $min_year)  || $year > $max_year){
            return false;
        }

        //Вычисляется и проверяется контрольное число введенного кода
        $value = $value * 1;
        $control_value = $value % 10;
        $min_reg = ( ( (int) $value ) % 11);

        return $min_reg == $control_value;

    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        // TODO: Implement message() method.
        return "Номер не является ОГРН";
    }
}