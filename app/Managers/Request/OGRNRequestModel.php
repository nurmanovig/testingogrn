<?php


namespace App\Managers\Request;


use App\Rules\PrimaryStateRegNumberRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OGRNRequestModel
{
    public static $errors = null;
    public static $data = [];

    private static $request = null;

    public static function init(Request $request){
        self::$request = $request;
        self::$data = $request->all();

        return new self();
    }

    public function validate(){
        $validator = Validator::make(self::$request->all(), self::rules(), self::messages());
        if($validator->fails()){
            self::$errors = $validator->errors();
            return false;
        }

        return true;
    }

    private static function rules(){
        return [
            'number' => [
                'required',
                new PrimaryStateRegNumberRule()
            ],
            'captcha' => [
                'required',
                'captcha'
            ]
        ];
    }

    private static function messages(){
        return [
            'required' => 'Заполнение обязательно',
            'captcha' => 'Каптча не правильная'
        ];
    }
}