<?php


namespace App\Managers\Request\interfaces;


use Illuminate\Http\Request;

interface IModelRequest
{
    public static function init(Request $request);

    public function validate();

    public static function rules();

    public static function messages();
}