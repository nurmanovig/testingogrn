<?php


namespace App\Managers;


use App\Models\CourseModel;

class CourseManager
{
    public static function getValue($date_req, $code){
        $model = CourseModel::where(['date_req' => $date_req, 'char_code' => $code])->first();
        if($model != null){
           return $model;
        }

        $values = CourseApi::getByValues([$code], ['date_req'=> $date_req]);
        if(!isset($values[$code])){
            return null;
        }

        $model = new CourseModel();
        $model->name = $values[$code]['name'];
        $model->value = $values[$code]['value'];
        $model->char_code = $code;
        $model->date_req = $date_req;
        $model->save();

        return $model;
    }
}