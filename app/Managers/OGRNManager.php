<?php


namespace App\Managers;


use App\Models\OGRNModel;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class OGRNManager
{
    public static function getNumberById($id){
        $model = OGRNModel::find($id);
        if(!$model){
            return false;
        }

        return $model->number;
    }

    public static function save($number){
        $model = new OGRNModel();
        $model->number = $number;
        $model->ip_address = request()->ip();
        $model->save();

        return $model->id;
    }
}