<?php


namespace App\Managers;


class CourseApi
{
    private static $url = "http://www.cbr.ru/scripts/XML_daily.asp";
    private static $params = [];

    public static function getByValues($codes, $params){
        $info = self::get($params);
        $_courses = [];

        if(isset($info['course'])){
            return $_courses;
        }

        foreach ($info['Valute'] as $key => $course){
            if(in_array($course['CharCode'], $codes))
                $_courses[$course['CharCode']] = [
                                                    'name' => $course['Name'],
                                                    'value' => str_replace(',', '.', $course['Value']) * 1
                                                 ];
        }

        return $_courses;
    }

    public static function get($params = []){
        self::$params = $params;
        $result = self::curl();
        return self::xmlToArray($result);
    }

    private static function xmlToArray($str){
        $xml = simplexml_load_string($str);
        $json = json_encode($xml);
        return json_decode($json,true);
    }

    public static function curl(){
        $url = self::$url . "?";
        foreach(self::$params as $key => $value){
            $url .= $key ."=".$value;
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 130);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}