<?php


namespace App\Http\Controllers;


use App\Helpers\HotKeys;
use App\Managers\CourseManager;
use App\Managers\OGRNManager;
use App\Managers\Request\CourseRequest;
use Illuminate\Http\Request;

class InfoController
{

    public function index(Request $request){
        $number = OGRNManager::getNumberById(session(HotKeys::OGRN_KEY));
        $course = null;
        if(CourseRequest::init($request)->validate()){
            $course = CourseManager::getValue(
                                        $request->get('date_req'),
                                        $request->get('code'));
        }

        return View('info.index', [
                                            'number' => $number,
                                            'course' => $course,
                                            'data' => $request->all(),
                                            'errors' => CourseRequest::$errors
                                        ]);
    }
}