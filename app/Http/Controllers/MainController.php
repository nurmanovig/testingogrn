<?php


namespace App\Http\Controllers;


use App\Helpers\HotKeys;
use App\Managers\OGRNManager;
use App\Managers\Request\OGRNRequestModel;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class MainController
{
    public function index(){
        session()->remove(HotKeys::OGRN_KEY);
        return view('main.index', self::getReturnValues());
    }

    public function set(Request $request){
        if(!OGRNRequestModel::init($request)->validate()){
            return view('main.index',
                self::getReturnValues(
                    OGRNRequestModel::$errors,
                    OGRNRequestModel::$data));
        }

        $id = OGRNManager::save($request->get('number'));
        session([HotKeys::OGRN_KEY => $id]);

        return redirect('/info');
    }

    private static function getReturnValues(MessageBag $errors = null, array $data = []){
        return compact('errors', 'data');
    }

}