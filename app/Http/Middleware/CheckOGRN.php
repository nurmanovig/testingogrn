<?php

namespace App\Http\Middleware;

use App\Helpers\HotKeys;
use Closure;

class CheckOGRN
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has(HotKeys::OGRN_KEY)){
            return redirect('/');
        }

        return $next($request);
    }
}
